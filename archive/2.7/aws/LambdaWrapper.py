# -*- coding: utf-8 -*-
import inspect

class Log:

    @staticmethod
    def info(msg):
        Log.__log(msg)

    @staticmethod
    def warn(msg):
        Log.__log(msg)

    @staticmethod
    def error(msg):
        Log.__log(msg)

    @staticmethod
    def critical(msg):
        Log.__log(msg)

    @staticmethod
    def debug(msg):
        Log.__log(msg)

    @staticmethod
    def __log(msg):
        current_frame = inspect.currentframe()
        calling_frame = inspect.getouterframes(current_frame, 2)
        name = calling_frame[1][3]
        print('[%s] %s' % (name, msg))


class Context:

    c = None

    def __init__(self, context):
        self.c = context

    def remaining_time(self):
        return self.c.get_remaining_time_in_millis()

    def name(self):
        return self.c.function_name

    def version(self):
        return self.c.function_name

    def invoked_func(self):
        return self.c.invoked_function_arn

    def mem_limit(self):
        return self.c.memory_limit_in_mb

    def request_id(self):
        return self.c.aws_request_id
    
    def log_group(self):
        return self.c.log_group_name

    def log_stream(self):
        return self.c.log_stream_name
