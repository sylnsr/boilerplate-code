# -*- coding: utf-8 -*-

from LambdaWrapper import Log, Context
import simplejson as json


def _jd(v):
    try:
        return json.dumps(v, use_decimal=True)
    except:
        return '%s' % v


def test(event, context):
    c = Context(context)
    Log.info("Name: " + c.name())
    Log.info("Invoked: " + c.invoked_func())
    Log.info("Version: " + c.version())
    return _jd(dict(Message='Hello World!', Event=event))
