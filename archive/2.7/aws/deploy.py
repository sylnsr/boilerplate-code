#!/usr/bin/env python
import os
import zipfile
import shutil
import boto3
import json

# these import are for inspecting what functions we need to deploy
import types
import Lambda

this_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(this_dir)


def add_to_zip(path, zip_h):
    for root, dirs, files in os.walk(path):
        for file in files:
            if root.startswith('./botocore') or root.startswith('./boto3'):
                continue
            else:
                zip_h.write(os.path.join(root, file))


def create_role():
    try:
        iam_client.get_role(RoleName='LambdaBasicExecution')
    except iam_client.exceptions.NoSuchEntityException:
        role_policy_document = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "lambda.amazonaws.com"
                    },
                    "Action": "sts:AssumeRole"
                }
            ]
        }
        iam_client.create_role(
            RoleName='LambdaBasicExecution',
            AssumeRolePolicyDocument=json.dumps(role_policy_document),
        )


def function_exists(name, functions):
    for fn in functions:
        if fn['FunctionName'] == name:
            return True
    return False


##########
# PACKAGE:
##########
# the zip file to upload to AWS Lambda
zip_h = zipfile.ZipFile('python-lambda.zip', 'w', zipfile.ZIP_DEFLATED)

# install dependencies
os.system('pip install -r requirements.txt -t ./tmp/packages/')
os.chdir(this_dir + '/tmp/packages/')
add_to_zip('./', zip_h)
os.chdir(this_dir)
shutil.rmtree(this_dir + '/tmp')

# then add the .py files in the project directory
for filename in os.listdir(this_dir):
    if filename.endswith(".py") and filename != 'deploy.py':
        zip_h.write(filename)
zip_h.close()

##########
# DEPLOY:
##########
handlers = []
for a in dir(Lambda):
    if isinstance(getattr(Lambda, a), types.FunctionType):
        if not a.startswith('_'):
            handlers.append(a)
client = boto3.client('lambda')
existing_functions = client.list_functions(FunctionVersion='ALL')['Functions']
iam_client = boto3.client('iam')
create_role()
role = iam_client.get_role(RoleName='LambdaBasicExecution')

with open('python-lambda.zip', 'rb') as f:
    zip_data = f.read()

for i in handlers:
    if function_exists(i, existing_functions):
        print("update function: " + i)
        result = client.update_function_code(
            FunctionName=i,
            ZipFile=zip_data,
            DryRun=False,
        )
    else:
        print("create function: Lambda." + i)
        result = client.create_function(
            FunctionName=i,
            Runtime='python2.7',
            Role=role['Role']['Arn'],
            Handler='Lambda.' + i,
            Code=dict(ZipFile=zip_data),
            Timeout=10,
        )
os.unlink('python-lambda.zip')
print(Lambda._jd(result))

##########
# TEST:
##########
print("invoke test:")
result = client.invoke(
    FunctionName='test',
    InvocationType='RequestResponse',
    Payload=b'%s' % Lambda._jd(dict(Test='Event', Foo='Bar'))
)
print(result['Payload'].read())
