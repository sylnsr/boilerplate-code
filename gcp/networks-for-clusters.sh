#!/bin/bash

# Get a list of projects
projects=$(gcloud projects list --format="value(projectId)")

# Iterate over projects
for project in $projects; do
    # Get a list of GKE clusters in the project
    clusters=$(gcloud container clusters list --project=$project --format="value(name)")

    # Check if each cluster is in "non-prod" or "prod" network
    for cluster in $clusters; do
        # Get the network configuration of the cluster
        network=$(gcloud container clusters describe $cluster --project=$project --format="value(network)")

        # Check if the network configuration contains "non-prod" or "prod"
        if echo $network | grep -q "non-prod"; then
            echo "$project: $cluster is in non-prod network"
        elif echo $network | grep -q "prod"; then
            echo "$project: $cluster is in prod network"
        fi
    done
done
