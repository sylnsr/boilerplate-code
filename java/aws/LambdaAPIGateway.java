package com.sylnsr.boilerplatecode.java.aws;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class LambdaAPIGateway {

    public Map<String, Object> handleRequest(Map<String, Object> request, Context context) {
        LambdaLogger logger = context.getLogger();
        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(request);
        Request req = gson.fromJson(jsonElement, Request.class);
        Map<String, Object> result = Respond(200, req.body, context);
        return result;
    }

    /**
     * Prepare a well formatted response for AWS API Gateway
     *
     * @param httpStatus
     * @param bodyAsJson
     * @param context
     * @return
     */
    public static Map<String, Object> Respond(int httpStatus, String bodyAsJson, Context context) {
        Map<String, Object> retval = new HashMap<>();
        /**
         Response MUST be in a specific format with "headers"; "statusCode" and "body" ONLY
         Example: { "headers": {"Content-Type":"application/json"}, "body":"...", "statusCode":200 }
         See http://amzn.to/2lY4oQB
         (under the heading Output Format of a Lambda Function for Proxy Integration)
         */
        Map<String, Object> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("x-request-id", context.getAwsRequestId());
        retval.put("headers", headers);
        retval.put("statusCode", httpStatus);
        retval.put("body", bodyAsJson);
        return retval;
    }


    public class Request {

        @SerializedName("resource")
        @Expose
        private String resource;
        @SerializedName("path")
        @Expose
        private String path;
        @SerializedName("httpMethod")
        @Expose
        private String httpMethod;
        @SerializedName("headers")
        @Expose
        private Object headers;
        @SerializedName("queryStringParameters")
        @Expose
        private Object queryStringParameters;
        @SerializedName("pathParameters")
        @Expose
        private Object pathParameters;
        @SerializedName("stageVariables")
        @Expose
        private Object stageVariables;
        @SerializedName("requestContext")
        @Expose
        private RequestContext requestContext;
        @SerializedName("body")
        @Expose
        private String body;
        @SerializedName("isBase64Encoded")
        @Expose
        private Boolean isBase64Encoded;

        public String getResource() {
            return resource;
        }

        public void setResource(String resource) {
            this.resource = resource;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getHttpMethod() {
            return httpMethod;
        }

        public void setHttpMethod(String httpMethod) {
            this.httpMethod = httpMethod;
        }

        public Object getHeaders() {
            return headers;
        }

        public void setHeaders(Object headers) {
            this.headers = headers;
        }

        public Object getQueryStringParameters() {
            return queryStringParameters;
        }

        public void setQueryStringParameters(Object queryStringParameters) {
            this.queryStringParameters = queryStringParameters;
        }

        public Object getPathParameters() {
            return pathParameters;
        }

        public void setPathParameters(Object pathParameters) {
            this.pathParameters = pathParameters;
        }

        public Object getStageVariables() {
            return stageVariables;
        }

        public void setStageVariables(Object stageVariables) {
            this.stageVariables = stageVariables;
        }

        public RequestContext getRequestContext() {
            return requestContext;
        }

        public void setRequestContext(RequestContext requestContext) {
            this.requestContext = requestContext;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public Boolean getIsBase64Encoded() {
            return isBase64Encoded;
        }

        public void setIsBase64Encoded(Boolean isBase64Encoded) {
            this.isBase64Encoded = isBase64Encoded;
        }

    }

    public class Identity {

        @SerializedName("cognitoIdentityPoolId")
        @Expose
        private Object cognitoIdentityPoolId;
        @SerializedName("accountId")
        @Expose
        private String accountId;
        @SerializedName("cognitoIdentityId")
        @Expose
        private Object cognitoIdentityId;
        @SerializedName("caller")
        @Expose
        private String caller;
        @SerializedName("apiKey")
        @Expose
        private String apiKey;
        @SerializedName("sourceIp")
        @Expose
        private String sourceIp;
        @SerializedName("accessKey")
        @Expose
        private String accessKey;
        @SerializedName("cognitoAuthenticationType")
        @Expose
        private Object cognitoAuthenticationType;
        @SerializedName("cognitoAuthenticationProvider")
        @Expose
        private Object cognitoAuthenticationProvider;
        @SerializedName("userArn")
        @Expose
        private String userArn;
        @SerializedName("userAgent")
        @Expose
        private String userAgent;
        @SerializedName("user")
        @Expose
        private String user;

        public Object getCognitoIdentityPoolId() {
            return cognitoIdentityPoolId;
        }

        public void setCognitoIdentityPoolId(Object cognitoIdentityPoolId) {
            this.cognitoIdentityPoolId = cognitoIdentityPoolId;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public Object getCognitoIdentityId() {
            return cognitoIdentityId;
        }

        public void setCognitoIdentityId(Object cognitoIdentityId) {
            this.cognitoIdentityId = cognitoIdentityId;
        }

        public String getCaller() {
            return caller;
        }

        public void setCaller(String caller) {
            this.caller = caller;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getSourceIp() {
            return sourceIp;
        }

        public void setSourceIp(String sourceIp) {
            this.sourceIp = sourceIp;
        }

        public String getAccessKey() {
            return accessKey;
        }

        public void setAccessKey(String accessKey) {
            this.accessKey = accessKey;
        }

        public Object getCognitoAuthenticationType() {
            return cognitoAuthenticationType;
        }

        public void setCognitoAuthenticationType(Object cognitoAuthenticationType) {
            this.cognitoAuthenticationType = cognitoAuthenticationType;
        }

        public Object getCognitoAuthenticationProvider() {
            return cognitoAuthenticationProvider;
        }

        public void setCognitoAuthenticationProvider(Object cognitoAuthenticationProvider) {
            this.cognitoAuthenticationProvider = cognitoAuthenticationProvider;
        }

        public String getUserArn() {
            return userArn;
        }

        public void setUserArn(String userArn) {
            this.userArn = userArn;
        }

        public String getUserAgent() {
            return userAgent;
        }

        public void setUserAgent(String userAgent) {
            this.userAgent = userAgent;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

    }


    public class RequestContext {

        @SerializedName("accountId")
        @Expose
        private String accountId;
        @SerializedName("resourceId")
        @Expose
        private String resourceId;
        @SerializedName("stage")
        @Expose
        private String stage;
        @SerializedName("requestId")
        @Expose
        private String requestId;
        @SerializedName("identity")
        @Expose
        private Identity identity;
        @SerializedName("resourcePath")
        @Expose
        private String resourcePath;
        @SerializedName("httpMethod")
        @Expose
        private String httpMethod;
        @SerializedName("apiId")
        @Expose
        private String apiId;

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public String getResourceId() {
            return resourceId;
        }

        public void setResourceId(String resourceId) {
            this.resourceId = resourceId;
        }

        public String getStage() {
            return stage;
        }

        public void setStage(String stage) {
            this.stage = stage;
        }

        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public Identity getIdentity() {
            return identity;
        }

        public void setIdentity(Identity identity) {
            this.identity = identity;
        }

        public String getResourcePath() {
            return resourcePath;
        }

        public void setResourcePath(String resourcePath) {
            this.resourcePath = resourcePath;
        }

        public String getHttpMethod() {
            return httpMethod;
        }

        public void setHttpMethod(String httpMethod) {
            this.httpMethod = httpMethod;
        }

        public String getApiId() {
            return apiId;
        }

        public void setApiId(String apiId) {
            this.apiId = apiId;
        }
    }
}