terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket = "improving-mls-tf-backend"
    encrypt = true
    key = "state.tfstate"
    region = "us-east-1"
    dynamodb_table = "tf-state-lock"
  }
}

provider "aws" {
  profile = var.aws_profile
  region = var.aws_region
}

resource "aws_s3_bucket" "lake_main" {
  bucket = "${var.global_res_name_prefix}-lake-main"
  lifecycle {
    prevent_destroy = false
  }
  tags = {
    source = "pipeline"
  }
}

