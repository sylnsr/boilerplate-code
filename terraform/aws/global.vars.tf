# AWS vars used here and in ./onetime
variable "aws_profile" {
  type = string
  description = "AWS credentials profile to use"
  default = "default"
}

variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "global_res_name_prefix" {
  type = string
  description = "Name prefix for resources that required globally unique names (e.g. S3 bucket name)"
  default = "improving-mls"
}