# AWS variables
# Note, vars also come from global.vars.tf which is linked from the root path
variable "bucket_name" {
  type = string
  default = "terraform-state-lock"
}
