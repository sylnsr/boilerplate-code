terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = var.aws_profile
  region = var.aws_region
}

resource "aws_s3_bucket" "bucket" {
  bucket = "${var.global_res_name_prefix}-tf-backend"
  lifecycle {
    prevent_destroy = true
  }
  tags = {
    source = "pipeline"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "bucket_sse" {
  bucket = aws_s3_bucket.bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

# It is highly recommended that you enable Bucket Versioning on the S3 bucket to allow for state recovery in the case of
# accidental deletions and human error.
resource "aws_s3_bucket_versioning" "terraform_state" {
  bucket = aws_s3_bucket.bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "block" {
  bucket = aws_s3_bucket.bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# Dynamo DB setup for state locking .. else we'll have no state locking!!
# (This has nothing to do with the S3 bucket config)
resource "aws_dynamodb_table" "terraform_state_lock" {
  name           = "tf-state-lock"
  hash_key       = "LockID"
  billing_mode = "PAY_PER_REQUEST"
  attribute {
    name = "LockID"
    type = "S"
  }
}

output "bucket_full_name" {
  value = "${var.global_res_name_prefix}-tf-backend"
}

output "dynamodb_table" {
  value = "tf-state-lock"
}