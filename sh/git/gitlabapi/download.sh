TOKEN=$1
PROJECT_ID=$2
URL="https://gitlab.com/api/v4/projects"

# Download the repo as a ZIP file
# Requires "read_api" scope
REPO_ZIP_URL="${URL}/${PROJECT_ID}/repository/archive.zip"
echo $REPO_ZIP_URL

curl -k --request GET \
--header PRIVATE-TOKEN:"${TOKEN}" "${REPO_ZIP_URL}" --output /tmp/${PROJECT_ID}.zip